package com.epam.learn.l1;

public class Test1 {
    private boolean paramA = true;
    private boolean paramB = false;
    private boolean paramC = true;

    public static void main(String[] args) {
        Test1 test1 = new Test1();
        System.out.println(test1.checkResult1(test1.paramA, test1.paramB, test1.paramC));
        System.out.println(test1.checkResult2(test1.paramA, test1.paramB, test1.paramC));
        System.out.println(test1.checkResult3(test1.paramA, test1.paramB, test1.paramC));
        System.out.println(test1.checkResult4(test1.paramA, test1.paramB, test1.paramC));
    }

    private boolean checkResult1 (boolean a, boolean b, boolean c){
        return a && b || c;
    }

    private boolean checkResult2 (boolean a, boolean b, boolean c){
        return a && b;
    }
    private boolean checkResult3 (boolean a, boolean b, boolean c){
        return c || b;
    }
    private boolean checkResult4 (boolean a, boolean b, boolean c){
        return a || b && c;
    }



}
